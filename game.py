from enum import Enum

class Brands(Enum):
	normal = 0
	fire = 1
	ice = 2
	water = 3
	grass = 4
	ground = 5
	dark = 6
	psycic = 7
	ghost = 8
	flying = 9
	fighting = 10

class Moves(Enum):
	#Normal Attack
	scratch = 0             #Scratches the Enemy              +5dmg
	slash = 1               #Slashes through the Foe          +7dmg
	hack = 2                #Hack the Foe into pieces         +10dmg
	whip = 3                #Attacks Random Times             +(4*RAND)dmg
	strike = 4              #Strikes hard.                    +15dmg
	destory = 5             #Annihilates the Foe.             +17dmg
	swipe = 6               #Swipes Enemy                     +7dmg
	fury = 7                #Fury Swipes                      +(5*RAND)dmg
	pound = 8               #Pounds Foe intensly              +10dmg
	jab = 9                 #Quick, Articulate Slice          +20dmg
	force = 10              #Forceful Punch                   +17dmg
	#Normal Defense
	shield = 11             #Blocks attack by .7              -70%atk:foe +5atk:self
	evade = 12
	parry = 13
	illude = 14
	facade = 15
	#Fire Attack
	flame = 16
	char = 17
	burn = 18
	flamethrower = 19
	ember = 20
	volcano = 21
	pire = 22
	overboil = 23
	overheat = 24
	spew = 25
	#Fire Defense
	smokescreen = 26
	heaten = 27


class Monster:
	
	#Initializer
	
	def __init__(self,name,brand,level,moves,stats,attribs):
		"""Initializes the Monster Class
		
		String Name
		String Brand (of enum BRAND)
		List Moves (of type ENUM/int)
		List Stats (of type ENUM/int)
		List Attrib (of type ENUM/int)
		
		"""
		self.name = name
		self.brand = brand
		self.level = level
		for a in moves:
			self.moves[a] = moves[a]
		for r in stats:
			self.stats[r] = stats[r]
		for g in attribs:
			self.attribs[g] = attribs[g]
			
	#Getter Classes
			
	def getName(self):
		"""Returns the Name of the Monster"""
		return self.name
		
	def getBrand(self):
		"""Returns the Type of the Monster"""
		return self.brand
		
	def getLevel(self):
		"""Returns The Level of the Monster"""
		return self.level
		
	def getMove(self,index):
		"""Returns a Specified Move"""
		return self.moves[index]
			
	def getMoves(self):
		"""Returns all Moves"""
		moves = []
		for i in self.moves:
			moves.append(i)
		return moves
		
	def getStat(self,index):
		"""Returns a Specified Move"""
		return self.stats[index]
			
	def getStats(self):
		"""Returns all Stats"""
		stats = []
		for i in self.stats:
			stats.append(i)
		return stats
			
	def getAttrib(self,index):
		"""Returns a Specified Attrib"""
		return self.attribs[index]
			
	def getAttribs(self):
		"""Returns all Attrib"""
		stats = []
		for i in self.attribs:
			attribs.append(i)
		return attribs
		
	#Setter Classes
	
	def setName(self,name):
		self.name = name
		
	def setBrand(self,brand):
		self.brand = brand
		
	def setMove(self,index,move):
		self.moves[index] = move
		
	def setStat(self,
	
		
